namespace Bnchmrk;

public static class StringExtensions
{
    public static bool SpanStartWith(this string s, string value, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
    {
        return s.AsSpan(0, value.Length).Equals(value, stringComparison);
    }

    unsafe public static void SpanReplace(this string s, Span<char> span, char oldValue, char newValue)
    {
        span.Clear();
        s.AsSpan().Replace(span, oldValue, newValue);
    }

    public static bool SpanContains(this string s, char value)
    {
        return s.AsSpan().Contains(value);
    }

    public static int SpanIndexOf(this string s, string value)
    {
        return s.AsSpan().IndexOf(value);
    }

    public static int SpanSplit(this string s, Span<Range> span, string separator)
    {
        return s.AsSpan().Split(span, separator);
    }

    public static int SpanToLowerInvariant(this string s, Span<char> span)
    {
        span.Clear();
        return s.AsSpan().ToLowerInvariant(span);
    }
}