using System.Text.Json;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;

namespace Bnchmrk;

[GroupBenchmarksBy(BenchmarkLogicalGroupRule.ByCategory)]
[CategoriesColumn]
[MemoryDiagnoser]
public class Benchmark
{
    private string[] _paths = [];

    private readonly string _json = "./../../../../../../../directories.json";

    [GlobalSetup]
    public void Setup()
    {
        using var stream = File.OpenRead(_json);
        _paths = JsonSerializer.Deserialize<string[]>(stream) ?? throw new InvalidOperationException();
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("Contains")]
    public void StringContains()
    {
        foreach (var path in _paths)
        {
            _ = path.Contains('\\');
        }
    }

    [Benchmark]
    [BenchmarkCategory("Contains")]
    public void SpanContains()
    {
        foreach (var path in _paths)
        {
            _ = path.SpanContains('\\');
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("Replace")]
    public void StringReplace()
    {
        foreach (var path in _paths)
        {
            _ = path.Replace('\\', '/');
        }
    }

    [Benchmark]
    [BenchmarkCategory("Replace")]
    public void SpanReplace()
    {
        Span<char> span = stackalloc char[1024];

        foreach (var path in _paths)
        {
            path.SpanReplace(span, '\\', '/');
        }
    }

    [Benchmark]
    [BenchmarkCategory("Replace")]
    public void SpanReplaceWithAllocation()
    {
        Span<char> span = stackalloc char[1024];

        foreach (var path in _paths)
        {
            path.SpanReplace(span, '\\', '/');
            _ = span[..path.Length].ToString();
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("StartsWith")]
    public void StringStartsWith()
    {
        foreach (var path in _paths)
        {
            _ = path.StartsWith("ADMIN Permission for Folder");
        }
    }

    [Benchmark]
    [BenchmarkCategory("StartsWith")]
    public void SpanStartWith()
    {
        foreach (var path in _paths)
        {
            _ = path.SpanStartWith("ADMIN Permission for Folder");
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("IndexOf")]
    public void StringIndexOf()
    {
        foreach (var path in _paths)
        {
            _ = path.IndexOf("Folder");
        }
    }

    [Benchmark]
    [BenchmarkCategory("IndexOf")]
    public void SpanIndexOf()
    {
        foreach (var path in _paths)
        {
            _ = path.SpanIndexOf("Folder");
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("Split")]
    public void StringSplit()
    {
        var max = 0;
        foreach (var path in _paths)
        {
            var count = path.Split("\\").Length;
            if (max < count) max = count;
        }
    }

    [Benchmark]
    [BenchmarkCategory("Split")]
    public void SpanSplit()
    {
        Span<Range> span = new(new Range[128]);
        var max = 0;
        foreach (var path in _paths)
        {
            var count = path.SpanSplit(span, "\\");
            if (max < count) max = count;
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("ToLower")]
    public void StringToLower()
    {
        foreach (var path in _paths)
        {
            _ = path.ToLowerInvariant();
        }
    }

    [Benchmark]
    [BenchmarkCategory("ToLower")]
    public void SpanToLower()
    {
        Span<char> span = stackalloc char[1024];

        foreach (var path in _paths)
        {
            var length = path.SpanToLowerInvariant(span);
        }
    }

    [Benchmark]
    [BenchmarkCategory("ToLower")]
    public void SpanToLowerWithAllocation()
    {
        Span<char> span = stackalloc char[1024];

        foreach (var path in _paths)
        {
            var length = path.SpanToLowerInvariant(span);
            _ = span[..length].ToString();
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("Trim")]
    public void StringTrim()
    {
        foreach (string path in _paths)
        {
            var _ = path.TrimEnd('\\');
        }
    }

    [Benchmark]
    [BenchmarkCategory("Trim")]
    public void SpanTrim()
    {
        foreach (string path in _paths)
        {
            var _ = path.AsSpan().TrimEnd('\\');
        }
    }

    [Benchmark]
    [BenchmarkCategory("Trim")]
    public void SpanTrimWithAllocation()
    {
        foreach (string path in _paths)
        {
            var _ = path.AsSpan().TrimEnd('\\').ToString();
        }
    }

    [Benchmark(Baseline = true)]
    [BenchmarkCategory("Substring")]
    public void StringSubstring()
    {
        foreach (string path in _paths)
        {
            _ = path[5..30];
        }
    }

    [Benchmark]
    [BenchmarkCategory("Substring")]
    public void SpanSubstring()
    {
        foreach (string path in _paths)
        {
            ReadOnlySpan<char> span = path.AsSpan();
            _ = span[5..30];
        }
    }

    [Benchmark]
    [BenchmarkCategory("Substring")]
    public void SpanSubstringWithAllocation()
    {
        foreach (string path in _paths)
        {
            ReadOnlySpan<char> span = path.AsSpan();
            _ = span[5..30].ToString();
        }
    }
}
